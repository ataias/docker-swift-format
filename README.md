# docker-swift-format

## Introduction

This repo contains a Dockerfile for containerizing
[swift-format](https://github.com/apple/swift-format) and a helper script
[build.sh](build.sh) to automate building it for AMD64 and ARM64.

The script relies on some environment variables being set, so make sure to
define them and source them. An example for using the current host Docker for
ARM64 and another host for AMD64 would be:

```fish
#!/usr/bin/env fish

set -x SWIFT_VERSION 5.5.2
set -x SWIFT_FORMAT_VERSION 0.50500.0
set -x BASE_REGISTRY ataias

set -x BASE_DEV_IMAGE $BASE_REGISTRY/swift:$SWIFT_VERSION-focal
set -x BASE_PROD_IMAGE $BASE_REGISTRY/swift:$SWIFT_VERSION-focal-slim
set -x PROD_IMAGE_PREFIX $BASE_REGISTRY/swift-format
set -x HOST_AMD64 ssh://lucy
```

Note: the example above is in fish shell syntax as I use fish and sourcing it is
easier that way, but you can easily convert that to bash. The helper script itself uses
bash.

After having the environment variables set up, you should be able to just run
`./build.sh`.

## Docker Image Usage

The command below is an example where you would format any Swift files inside
the current directory and sub-directories, changing them in place (make sure to
have some versioning so you don't have unexpected changes).

```sh
docker run --volume $(PWD):/project --workdir /project ataias/swift-format swift-format format --in-place **/*.swift
```

Or, on fish:

```sh
docker run --volume (PWD):/project --workdir /project ataias/swift-format swift-format format --in-place **/*.swift
```

For more usage examples, you should check `swift-format -h` and the [official repo](https://github.com/apple/swift-format).

## FAQ

### What is ssh://lucy ?

- `lucy` is a computer defined in my `~/.ssh/config`. It is an AMD64 machine
  that I use to build the AMD64 version, as I am using an Apple Silicon machine, with which I build the ARM64 version.

### Why two hosts instead of QEMU for building?

I tried QEMU so that I could build everything from an ARM or AMD machine and I
always got a core dumped or similar. Pretty bad experience, so ... two machines
it is.

## Links

- [docker-swift-format GitLab Repository](https://gitlab.com/ataias/docker-swift-format/)
- [docker-swift-format Docker Hub](https://hub.docker.com/r/ataias/swift-format)

## License

MIT. See [LICENSE](LICENSE).
