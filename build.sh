#!/usr/bin/env bash

set -e # exit on error
set -x # print commands as they are executed

SWIFT_FORMAT_VERSION=${SWIFT_FORMAT_VERSION:-0.50500.0}
SWIFT_VERSION=${SWIFT_VERSION:-5.5.2}
BASE_DEV_IMAGE=${BASE_DEV_IMAGE:-ataias/swift:${SWIFT_VERSION}-focal}
BASE_PROD_IMAGE=${BASE_PROD_IMAGE:-ataias/swift:${SWIFT_VERSION}-focal}
PROD_IMAGE_PREFIX=${PROD_IMAGE_PREFIX:-ataias/swift-format}
PROD_IMAGE=$PROD_IMAGE_PREFIX:${SWIFT_FORMAT_VERSION}
PROD_IMAGE_LATEST=$PROD_IMAGE_PREFIX:latest
HOST_ARM64=${HOST_ARM64:-unix:///var/run/docker.sock}
HOST_AMD64=${HOST_AMD64:-unix:///var/run/docker.sock}

cd $(git rev-parse --show-toplevel)

rm -rf .source-code

wget -c https://github.com/apple/swift-format/archive/refs/tags/${SWIFT_FORMAT_VERSION}.tar.gz
tar xf ${SWIFT_FORMAT_VERSION}.tar.gz
mv swift-format-${SWIFT_FORMAT_VERSION} .source-code

cp Dockerfile .source-code/
cd .source-code

docker --host $HOST_ARM64 \
    build \
    -t $PROD_IMAGE-arm64 \
    --build-arg BUILDER_IMAGE=$BASE_DEV_IMAGE \
    --build-arg RUNTIME_IMAGE=$BASE_PROD_IMAGE \
    --platform linux/arm64 \
    .

docker --host $HOST_AMD64 \
    build \
    -t $PROD_IMAGE-amd64 \
    --build-arg BUILDER_IMAGE=$BASE_DEV_IMAGE \
    --build-arg RUNTIME_IMAGE=$BASE_PROD_IMAGE \
    --platform linux/amd64 \
    .

docker --host $HOST_ARM64 push $PROD_IMAGE-arm64
docker --host $HOST_AMD64 push $PROD_IMAGE-amd64

docker manifest create -a $PROD_IMAGE \
    $PROD_IMAGE-arm64 \
    $PROD_IMAGE-amd64

docker manifest push $PROD_IMAGE

docker manifest create -a $PROD_IMAGE_LATEST \
    $PROD_IMAGE-arm64 \
    $PROD_IMAGE-amd64

docker manifest push $PROD_IMAGE_LATEST
