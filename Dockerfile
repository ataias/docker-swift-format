ARG BUILD_IMAGE
ARG RUN_IMAGE
# ================================
# Build image
# ================================
FROM ${BUILD_IMAGE:-ataias/swift:5.5.2-focal} as build

# Install OS updates
RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true \
    && apt-get -q update \
    && apt-get -q dist-upgrade -y \
    && rm -rf /var/lib/apt/lists/*

# Set up a build area
WORKDIR /build

# First just resolve dependencies.
# This creates a cached layer that can be reused
# as long as your Package.swift/Package.resolved
# files do not change.
COPY ./Package.* ./
RUN swift package resolve

# Copy entire repo into container
COPY . .

# Build everything, with optimizations
RUN swift build -c release

# Switch to the staging area
WORKDIR /staging

# Copy main executable to staging area
RUN cp "$(swift build --package-path /build -c release --show-bin-path)/swift-format" ./

# ================================
# Run image
# ================================
FROM ${RUN_IMAGE:-ataias/swift:5.5.2-focal-slim}

# Make sure all system packages are up to date.
RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && \
    apt-get -q update && apt-get -q dist-upgrade -y && rm -r /var/lib/apt/lists/*

# Create a swift-format user and group with /app as its home directory
RUN useradd --user-group --create-home --system --skel /dev/null --home-dir /app swift-format

# Switch to the new home directory
WORKDIR /app

# Copy built executable and any staged resources from builder
COPY --from=build --chown=swift-format:swift-format /staging /usr/bin/

# Ensure all further commands run as the swift-format user
USER swift-format:swift-format

CMD ["swift-format"]
